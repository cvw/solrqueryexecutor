import java.util.ArrayList;
import java.util.List;

/**
 * User: csj
 * Date: 16/01/2013
 * Time: 7:24 AM
 */
public class Stat
{
        public long count;
        public boolean facetsOn;
        public boolean groupsOn;
        public boolean highlightsOn;

        public List<Integer> requestTimes = new ArrayList<Integer>();
        public long totalTime;
        public long rowsFound;

        @Override
        public String toString()
        {
                final StringBuilder sb = new StringBuilder("Stat{");
                sb.append("count=").append(count);
                sb.append(", totalTime=").append(totalTime);
                sb.append(", [");
                if (facetsOn)
                {
                        sb.append("F");
                }
                if (groupsOn)
                {
                        sb.append("G");
                }
                if (highlightsOn)
                {
                        sb.append("H");
                }
                sb.append("]");
                if (requestTimes.size() > 0)
                {
                        sb.append(", FIRST=").append(requestTimes.get(0));
                        sb.append(", AVG=").append((totalTime-requestTimes.get(0)) / (count - 1));
                }
                sb.append(", rowsFound=").append(rowsFound);
                sb.append('}');
                return sb.toString();
        }
}
