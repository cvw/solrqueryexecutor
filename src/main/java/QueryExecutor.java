import net.infopaq.searchservice.api.QueryClient;
import net.infopaq.searchservice.model.Query;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.GroupParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: csj
 * Date: 16/01/2013
 * Time: 6:20 AM
 */
@Component
public class QueryExecutor
{
        @Autowired
        private QueryClient queryClient;

        private static Logger log = LoggerFactory.getLogger(QueryExecutor.class);

        public void executeQuery(SolrQuery solrQuery, Stat stat)
        {
//                log.debug("Executing: " + stat.toString());
                String baseURL = "http://ixisolr03:41315/master-2013-04-15";
                HttpSolrServer solrServer = new HttpSolrServer(baseURL);


                QueryResponse response = null;
                try
                {
//                        log.debug(String.format("Executing request: %s", URLDecoder.decode(solrQuery.toString())));
                        int repeats = 100;
                        for (int i=0; i< repeats; i++)
                        {
                                response = solrServer.query(solrQuery);
                                stat.requestTimes.add(response.getQTime());
                                stat.totalTime += response.getQTime();
                        }
                        if (stat.groupsOn)
                        {
                                List<GroupCommand> groupValues = response.getGroupResponse().getValues();
                                if (groupValues != null && groupValues.size() > 0)
                                {
                                        GroupCommand group = groupValues.get(0);
                                        stat.rowsFound = group.getMatches();
                                }
                        }
                        else
                        {
                                stat.rowsFound = response.getResults().getNumFound();
                        }

                        stat.count = repeats;
                }
                catch (SolrServerException e)
                {
                        throw new RuntimeException(String.format("Error calling solr"), e);
                }
        }

        public void measureQuery(int queryId)
        {
                Query query = queryClient.get(queryId);
                queryClient.validate(query);
                String queryString = queryClient.getSolrQueryString(query);
                log.debug(String.format("Executing %s", queryString));
                // solrQuery.setParam("debug", "timing");

                // Raw
                SolrQuery solrQuery = getBaseSolrQuery(queryString);
                Stat stat = new Stat();
                executeQuery(solrQuery, stat);
                log.debug(stat.toString());
                // Raw + facets
                solrQuery = getBaseSolrQuery(queryString);
                addFacetsToQuery(solrQuery);
                stat = new Stat();
                stat.facetsOn = true;
                executeQuery(solrQuery, stat);
                log.debug(stat.toString());
                // Raw + groups
                solrQuery = getBaseSolrQuery(queryString);
                addGroupsToQuery(solrQuery);
                stat = new Stat();
                stat.groupsOn = true;
                executeQuery(solrQuery, stat);
                log.debug(stat.toString());
                // Raw + highlighting
                solrQuery = getBaseSolrQuery(queryString);
                addHighlightsToQuery(solrQuery);
                stat = new Stat();
                stat.highlightsOn = true;
                executeQuery(solrQuery, stat);
                log.debug(stat.toString());
                // Raw + facets and groups
                solrQuery = getBaseSolrQuery(queryString);
                addFacetsToQuery(solrQuery);
                addGroupsToQuery(solrQuery);
                stat = new Stat();
                stat.facetsOn = true;
                stat.groupsOn = true;
                executeQuery(solrQuery, stat);
                log.debug(stat.toString());
                // Raw + facets and groups and highlighting
                solrQuery = getBaseSolrQuery(queryString);
                addFacetsToQuery(solrQuery);
                addGroupsToQuery(solrQuery);
                addHighlightsToQuery(solrQuery);
                stat = new Stat();
                stat.facetsOn = true;
                stat.groupsOn = true;
                stat.highlightsOn = true;
                executeQuery(solrQuery, stat);
                log.debug(stat.toString());
                // Raw
                solrQuery = getBaseSolrQuery(queryString);
                stat = new Stat();
                executeQuery(solrQuery, stat);
                log.debug(stat.toString());

        }

        String shardList = "ixisolr03.infopaq.net:41301/master-2013-04-01,ixisolr03.infopaq.net:41315/master-2013-03-15,ixisolr03.infopaq.net:41301/master-2013-03-01,ixisolr03.infopaq.net:41315/master-2013-02-15,ixisolr03.infopaq.net:41301/master-2013-02-01,ixisolr03.infopaq.net:41315/master-2013-01-15,ixisolr03.infopaq.net:41301/master-2013-01-01";
        String shortShardList = "ixisolr03.infopaq.net:41301/master-2013-04-01,ixisolr03.infopaq.net:41315/master-2013-03-15";

        private SolrQuery getBaseSolrQuery(String queryString)
        {
                SolrQuery solrQuery = new SolrQuery(queryString);
                solrQuery.setParam("q.op", "OR");
                solrQuery.setParam("shards", shortShardList);
                return solrQuery;
        }


        public void addFacetsToQuery(SolrQuery solrQuery)
        {
                // Calculate a facet on how many articles was published in the last 24 hours
                solrQuery.setFacet(true);
                solrQuery.add("f.publishedDate.facet.range.start", "NOW/DAY");
                solrQuery.add("f.publishedDate.facet.range.end", "NOW/DAY+1DAY");
                solrQuery.add("f.publishedDate.facet.range.gap", "+24HOURS");
                solrQuery.add("facet.range", "publishedDate");

        }

        public void addGroupsToQuery(SolrQuery solrQuery)
        {
                solrQuery.set(GroupParams.GROUP, true);
                solrQuery.set(GroupParams.GROUP_FIELD, "clusterId");
                solrQuery.set(GroupParams.GROUP_TOTAL_COUNT, true);
        }

        public void addHighlightsToQuery(SolrQuery solrQuery)
        {
                solrQuery.setHighlight(true);
                solrQuery.setParam("hl.useFastVectorHighlighter", true);
                solrQuery.setParam("hl.usePhraseHighlighter", true);
                solrQuery.setParam("hl.boundaryScanner", "breakIterator");
                solrQuery.setParam("hl.fragsize", String.valueOf(1000 * 1000));
        }

        public static void main(String[] args)
        {
                System.getProperties().setProperty("searchservice.jms.endpoint", "tcp://vbrnobi.dmz.infopaq.net:6000");
                System.getProperties().setProperty("searchservice.jms.timeout", "5000");
                ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

                QueryExecutor queryExecutor = context.getBean("queryExecutor", QueryExecutor.class);

                Map<Integer, String> queries = new HashMap<Integer, String>();
//                queries.put(5386, "E.on Norge Web");
//                queries.put(5218, "Umeå");
//                queries.put(6049, "Carlsberg Sport - Web");
//                queries.put(4627, "IKEA PS");
//                queries.put(4650, "Telia");
//                queries.put(243, "Pillow Cake_Print_39633");
//                queries.put(2054, "Arbetsmarknaden");
//                queries.put(5396, "Värme Web");
//                queries.put(5383, "Elnät web");
//                queries.put(8785, "ALMA alla sökord SV 2012");

//                queries.put(5765, "Sverige - Web");
//                queries.put(7469, "Dagligstue (Ikea)");
                queries.put(7459, "Soveværelse (Ikea)");

                for (Integer queryId : queries.keySet())
                {
                        String name = queries.get(queryId);
                        log.debug(String.format("Executing %s with queryId %s", name, queryId));
                        queryExecutor.measureQuery(queryId);
                }


        }
}
